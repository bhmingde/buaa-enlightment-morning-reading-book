\setlength\parskip{.4\baselineskip}

\chapter{正心第一}

\section{论大医精诚}
张湛曰：夫经方之难精，由来尚矣。今病有内同而外异，亦有内异而外同，故五脏六腑之盈虚，血脉荣卫之通塞，固非耳目之所察，必先诊候以审之。而寸口关尺，有浮沉弦紧之乱；腧穴流注，有高下浅深之差；肌肤筋骨，有厚薄刚柔之异。唯用心精微者，始可与言于兹矣。今以至精至微之事，求之于至粗至浅之思，其不殆哉！若盈而益之，虚而损之，通而彻之，塞而壅之，寒而冷之，热而温之，是重加其疾，而望其生，吾见其死矣。故医方卜筮，艺能之难精者也。既非神授，何以得其幽微？世有愚者，读方三年，便谓天下无病可治；及治病三年，乃知天下无方可用。故学人必须博极医源，精勤不倦，不得道听途说，而言医道已了，深自误哉。

凡大医治病，必当安神定志，无欲无求，先发大慈恻隐之心，誓愿普救含灵之苦。若有疾厄来求救者，不得问其贵贱贫富，长幼妍媸，怨亲善友，华夷愚智，普同一等，皆如至亲之想。亦不得瞻前顾后，自虑吉凶，护惜身命。见彼苦恼，若己有之，深心凄怆，勿避险巇，昼夜寒暑，饥渴疲劳，一心赴救，无作功夫形迹之心，如此可为苍生大医，反此则是含灵巨贼。

自古名贤治病，多用生命以济危急，虽曰贱畜贵人，至于爱命，人畜一也，损彼益己，物情同患，况于人乎？夫杀生求生，去生更远。吾今此方，所以不用生命为药者，良由此也。其虻虫、水蛭之属，市有先死者，则市而用之，不在此例。只如鸡卵一物，以其混沌未分，必有大段要急之处，不得已隐忍而用之。能不用者，斯为大哲，亦所不及也。

其有患疮痍下痢，臭秽不可瞻视，人所恶见者，但发惭愧、凄怜、忧恤之意，不得起一念蒂芥之心，是吾之志也。

夫大医之体，欲得澄神内视，望之俨然；宽裕汪汪，不皎不昧；省病诊疾，至意深心；详察形候，纤毫勿失；处判针药，无得参差。虽曰病宜速救，要须临事不惑，唯当审谛覃\footnote{覃（tán）。覃思：深思。}思，不得于性命之上，率尔自逞俊快，邀射名誉，甚不仁矣。又到病家，纵绮罗满目，勿左右顾眄；丝竹凑耳，无得似有所娱；珍馐迭荐，食如无味；醽醁兼陈，看有若无。所以尔者，夫一人向隅，满堂不乐，而况病人苦楚，不离斯须，而医者安然欢娱，傲然自得，兹乃人神之所共耻，至人之所不为，斯盖(盍)医之本意也。

夫为医之法，不得多语调笑，谈谑喧哗，道说是非，议论人物，炫耀声名，訾毁诸医，自矜己德。偶然治瘥一病，则昂头戴面，而有自许之貌，谓天下无双。此医人之膏肓也。老君曰：人行阳德，人自报之；人行阴德，鬼神报之；人行阳恶，人自报之；人行阴恶，鬼神害之。寻此二途，阴阳报施，岂诬也哉。所以医人不得恃己所长，专心经略财物，但作救苦之心，于冥运道中，自感多福者耳。又不得以彼富贵，处以珍贵之药，令彼难求，自炫功能，谅非忠恕之道。志存救济，故亦曲碎论之，学者不可耻言之鄙俚也。

\section{伤寒杂病论序}
论曰：余每览越人入虢之诊，望齐侯之色，未尝不慨然叹其才秀也。怪当今居世之士，曾不留神医药，精究方术，上以疗君亲之疾，下以救贫贱之厄，中以保身长全，以养其生。但竞逐荣势，企踵权豪，孜孜汲汲，惟名利是务，崇饰其末，忽弃其本，华其外而悴其内。皮之不存，毛将安附焉？卒然遭邪风之气，婴非常之疾，患及祸至，而方震栗，降志屈节，钦望巫祝，告穷归天，束手受败。赍百年之寿命，持至贵之重器，委付凡医，恣其所措。咄嗟呜呼！厥身已毙，神明消灭，变为异物，幽潜重泉，徒为啼泣。痛夫！举世昏迷，莫能觉悟，不惜其命，若是轻生，彼何荣势之云哉！而进不能爱人知人，退不能爱身知己，遇灾值祸，身居厄地，蒙蒙昧昧，惷若游魂。哀乎！趋世之士，驰竞浮华，不固根本，忘躯徇物，危若冰谷，至于是也！

余宗族素多，向余二百，建安纪年以来，犹未十稔，其死亡者，三分有二，伤寒十居其七。感往昔之沦丧，伤横夭之莫救，乃勤求古训，博采众方，撰用《素问》《九卷》《八十一难》《阴阳大论》《胎胪药录》，并平脉辨证，为《伤寒杂病论》，合十六卷。虽未能尽愈诸病，庶可以见病知源，若能寻余所集，思过半矣。

夫天布五行，以运万类；人禀五常，以有五脏。经络府俞，阴阳会通；玄冥幽微，变化难极。自非才高识妙，岂能探其理致哉！上古有神农、黄帝、岐伯、伯高、雷公、少俞、少师、仲文，中世有长桑、扁鹊，汉有公乘阳庆及仓公。下此以往，未之闻也。观今之医，不念思求经旨，以演其所知，各承家技，终始顺旧，省疾问病，务在口给，相对斯须，便处汤药。按寸不及尺，握手不及足；人迎趺阳，三部不参；动数发息，不满五十。短期未知决诊，九候曾无仿佛；明堂阙庭，尽不见察，所谓窥管而已。夫欲视死别生，实为难矣。

孔子云：生而知之者上，学则亚之。多闻博识，知之次也。余宿尚方术，请事斯语。

\section{论大医习业}
凡欲为大医，必须谙《素问》、《甲乙》、《黄帝针经》、明堂流注、十二经脉、三部九候、五脏六腑、表里孔穴、本草药对，张仲景、王叔和、阮河南、范东阳、张苗、靳邵等诸部经方。
又须妙解阴阳禄命，诸家相法，及灼龟五兆、《周易》六壬，并须精熟，如此乃得为大医。若不尔者，如无目夜游，动致颠殒。

次须熟读此方，寻思妙理，留意钻研，始可与言于医道者矣。又须涉猎群书，何者？若不读五经，不知有仁义之道。不读三史，不知有古今之事。不读诸子，睹事则不能默而识之。不读《内经》，则不知有慈悲喜舍之德。不读《庄》《老》，不能任真体运，则吉凶拘忌，触涂而生。至于五行休王，七曜天文，并须探赜。若能具而学之，则于医道无所滞碍，尽善尽美矣。

\section{袁氏医家十事}
% \chooseFrom{明·王绍隆}{医灯续焰}

一、医之志。须发慈悲恻隐之心，誓救大地含灵之苦。视众生之病，不论亲疏贵贱，贤愚贫富，皆当恫瘝乃身，尽力殚力，曲为拯理。

二、医之学。须上通天道，使五运六气，变化郁复之理，无一不精。中察人身，使十四经络，内而五脏六腑之渊涵，外而四肢百骸之贯串，无一不彻。下明物理，使昆虫草木之性情气味，无一不畅。然后可以识病而用药。

三、医之识。医之用药，如将之用兵。纵横合变，呼吸异宜。非识见之高，不能神会而独断也。然此识非可袭取，非可商量，全在方寸中，虚明活泼。须涤除嗜欲，恬澹无为，则虚空自然生白也。

四、医之慎。医为人之司命，生死系之。用药之际，须兢兢业业，不可好奇而妄投一药，不可轻人命而擅试一方，不可骋聪明而遽违古法。倘或稍误，明有人非，幽有鬼责，可惧也。

五、医之善。君子之游艺，与据德依仁，皆为实学。故古人技艺之工，都从善养中得来。若承蜩，若养鸡，皆是法也。医虽小道，实具甚深三昧。须收摄心体，涵泳性灵，动中习存，忙中习定。外则四体常和，内则元神常寂。然后望色闻声，问病切脉。自然得其精，而施治得宜也。

六、医之术。医非徒仁术，亦仙术也。谚云：古来医道通仙道。此岂无稽之言哉？凡欲学医，须将玄门之旨，留神讲究。玄牝之门，生身之户，守中养气之诀，观窍观妙之理，务求明师指示，亲造其藩而闯其室。此处看得明白，则病候之生灭，身中之造化，已洞悉矣。以之治疾，岂不易易。况人之疾，有草木金石所不能治者，则教之依法用功，无不立愈。天台智者禅师，谓一日一夜调息之功，可以已二十余年之痼疾。盖天之阳气一回，则万物生色。人之元气一复，则百体皆和。宿疾普消，特其余事耳。

七、医之量。书云：必有忍，其乃有济。有容德乃大。医者术业既高，则同类不能无忌。识见出众，则庸庶不能无疑。疑与忌合，而诽谤指摘，无所不至矣。须容之于不校，付之于无心，而但尽力于所事。间有排挤殴詈，形之辞色者，亦须以孟子三自反之法应之。彼以逆来，我以顺受，处之超然，待之有礼，勿使病家动念可也。

八、医之言。仲尼大圣，屡以慎言为训。而医者之言，尤当慎者。不可夸己之长，不可谈人之短，不可浮诞而骇惑病人，不可轻躁而诋诽同类。病情之来历，用药之权衡，皆当据实晓告，使之安心调理。不可诬轻为重，不可诳重为轻。即有不讳，亦须委曲明谕。病未剧，则宽以慰之，使安心调理；病既剧，则示以全归之道，使心意泰然。宁默毋哗，宁慎毋躁。

九、医之行。语曰：以身教之从，以言教之讼。故慎吾之言，不若端吾之行。道高天下，守之以谦；智绝人群，处之以晦。敦孝弟，重伦理，而于礼、义、廉、耻四字，则秉之如蓍龟，遵之如柱石。久而勿失，自然起敬起信，而医道易行也。

十、医之守。医虽为养家，尤须以不贪为本。凡有病人在，即举家不宁。当此时而勒人酬谢，家稍不足，则百计营求，艰难更倍。即充足之家，亦于满堂懊恼之中，而受其谘诅痛苦之惠，亦非心之所安也。故我生平于病人所馈，不敢纤毫轻受。有不给者，或更多方周给之。非以市恩，吾尽吾心而已矣。子孙习医，而能依此十事，古之圣贤，何以加此。

\chapter{养生第二}
\section{孙真人卫生歌}
天地之间人为贵，头象天兮足象地，\\
父母遗体宜宝之，洪范五福寿为最。

卫生且要知三戒，大怒大欲并大醉，\\
三者若还有一焉，须防损失真元气。\\
欲求长生须戒性，火不出兮心自定，\\
木还去火不成灰，人能戒性还延命。\\
贪欲无穷忘却精，用心不已失元神。\\
劳形散尽中和气，更仗何能保此身。\\
心若太费费则竭，形若太劳劳则怯，\\
神若太伤伤则虚，气若太损损则绝。\\
世人欲识卫生道，喜乐有常嗔怒少，\\
心诚意正思虑除，顺理修身去烦恼。

春嘘明目夏呵心，秋呬冬吹肺肾宁，\\
四季常呼脾化食，三焦嘻出热难停。\\
发宜多梳气宜炼，齿宜数叩津宜咽，\\
子欲不死修昆仑，双手揩摩常在面。

春月少酸宜食甘，冬月宜苦不宜咸，\\
夏日增辛宜减苦，秋来辛减略加酸，\\
季月少咸甘略戒，自然五脏保平安。\\
若能全减身康健，滋味偏多无病难。

春寒莫放绵衣薄，夏月汗多宜换着，\\
秋冬觉冷便加添，莫待病生才服药。\\
唯有夏月难调理，伏阴在内忌冰水，\\
瓜桃生冷宜少餐，免至秋来成疟痢。\\
心旺肾衰色宜避，养肾固精当节制，\\
常令肾实不虚空，自然强健无忧虑。

大饱伤脾饥伤胃，大渴伤血多伤气，\\
饥餐渴饮莫太过，免致膨脝损心肺。\\
醉后强饮饱强食，未有此身不生疾。\\
人资饮食以养生，去其甚者将安适。\\
食后徐行百步多，手摩脐腹食消磨。\\
夜半灵根灌清水，丹田浊气切须呵。\\
饮酒可以陶情性，大饮过多防百病。\\
肺为华盖倘受伤，咳嗽劳神能损命。\\
慎勿将盐来点茶，分明引贼入肾家。\\
下焦虚冷令人瘦，伤肾伤脾防病加。\\
坐卧防风来脑后，脑后受风人不寿，\\
更兼醉饱卧风中，风入五内成灾咎。

雁有序兮犬有义，黑鲤朝北知臣礼，\\
人无礼义反食之，天地神明终不喜。

养体须当节五辛，五辛不节反伤身，\\
莫教引动虚阳发，精竭荣枯病渐侵。\\
不问在家并在外，若遇迅雷风雨大，\\
急宜端肃畏天威，静坐澄心须谨戒。

恩爱牵缠不自由，利名萦绊几时休，\\
放宽些子自家福，免致中年早白头。\\
顶天立地非容易，饱食暖衣宁不愧，\\
思量无以报洪恩，晨夕焚香频忏悔。\\
身安寿永福如何，胸次平夷积善多，\\
惜命惜身兼惜气，请君熟玩卫生歌。

\section{上古天真论}
昔在黄帝，生而神灵，弱而能言，幼而徇齐，长而敦敏，成而登天。乃问于天师曰：余闻上古之人，春秋皆度百岁，而动作不衰；今时之人，年半百而动作皆衰者，时世异耶？人将失之耶？岐伯对曰：上古之人，其知道者，法于阴阳，和于术数，食饮有节，起居有常，不妄作劳，故能形与神俱，而尽终其天年，度百岁乃去。今时之人不然也，以酒为浆，以妄为常，醉以入房，以欲竭其精，以耗散其真，不知持满，不时御神，务快其心，逆于生乐，起居无节，故半百而衰也。

夫上古圣人之教下也，皆谓之虚邪贼风，避之有时，恬惔虚无，真气从之，精神内守，病安从来。是以志闲而少欲，心安而不惧，形劳而不倦，气从以顺，各从其欲，皆得所愿。故美其食，任其服，乐其俗，高下不相慕，其民故曰朴。是以嗜欲不能劳其目，淫邪不能惑其心，愚智贤不肖，不惧于物，故合于道。所以能年皆度百岁而动作不衰者，以其德全不危也。

\setlength\parskip{.1\baselineskip}
帝曰：人年老而无子者，材力尽邪？将天数然也？岐伯曰：女子七岁，肾气盛，齿更发长。

二七，而天癸至，任脉通，太冲脉盛，月事以时下，故有子。

三七，肾气平均，故真牙生而长极。

四七，筋骨坚，发长极，身体盛壮。

五七，阳明脉衰，面始焦，发始堕。

六七，三阳脉衰于上，面皆焦，发始白。

七七，任脉虚，太冲脉衰少，天癸竭，地道不通，故形坏而无子也。

丈夫八岁，肾气实，发长齿更。

二八，肾气盛，天癸至，精气溢泻，阴阳和，故能有子。

三八，肾气平均，筋骨劲强，故真牙生而长极。

四八，筋骨隆盛，肌肉满壮。

五八，肾气衰，发堕齿槁。

六八，阳气衰竭于上，面焦，发鬓颁白。

七八，肝气衰，筋不能动，天癸竭，精少，肾脏衰，形体皆极。

八八，则齿发去。
\setlength\parskip{.4\baselineskip}

肾者主水，受五脏六腑之精而藏之，故五脏盛，乃能泻。今五脏皆衰，筋骨解堕，天癸尽矣，故发鬓白，身体重，行步不正，而无子耳。帝曰：有其年已老，而有子者，何也？岐伯曰：此其天寿过度，气脉常通，而肾气有余也。此虽有子，男子不过尽八八，女子不过尽七七，而天地之精气皆竭矣。

帝曰：夫道者年皆百岁，能有子乎？岐伯曰：夫道者能却老而全形，身年虽寿，能生子也。

黄帝曰：余闻上古有真人者，提挈天地，把握阴阳，呼吸精气，独立守神，肌肉若一，故能寿敝天地，无有终时，此其道生。

中古之时，有至人者，淳德全道，和于阴阳，调于四时，去世离俗，积精全神，游行天地之间，视听八远之外，此盖益其寿命而强者也，亦归于真人。

其次有圣人者，处天地之和，从八风之理，适嗜欲于世俗之间，无恚嗔之心，行不欲离于世，被服章，举不欲观于俗，外不劳形于事，内无思想之患，以恬愉为务，以自得为功，形体不敝，精神不散，亦可以百数。

其次有贤人者，法则天地，象似日月，辨列星辰，逆从阴阳，分别四时，将从上古合同于道，亦可使益寿而有极时。

\section{四气调神大论}

春三月，此谓发陈，天地俱生，万物以荣，夜卧早起，广步于庭，被发缓形，以使志生，生而勿杀，予而勿夺，赏而勿罚，此春气之应，养生之道也。逆之则伤肝，夏为寒变，奉长者少。

夏三月，此谓蕃秀，天地气交，万物华实，夜卧早起，无厌于日，使志无怒，使华英成秀，使气得泄，若所爱在外，此夏气之应，养长之道也。逆之则伤心，秋为痎疟，奉收者少，冬至重病。

秋三月，此谓容平，天气以急，地气以明，早卧早起，与鸡俱兴，使志安宁，以缓秋刑，收敛神气，使秋气平，无外其志，使肺气清，此秋气之应，养收之道也。逆之则伤肺，冬为飧泄，奉藏者少。

冬三月，此谓闭藏，水冰地坼，无扰乎阳，早卧晚起，必待日光，使志若伏若匿，若有私意，若已有得，去寒就温，无泄皮肤，使气亟夺，此冬气之应，养藏之道也。逆之则伤肾，春为痿厥，奉生者少。

天气清净，光明者也，藏德不止，故不下也。天明则日月不明，邪害空窍，阳气者闭塞，地气者冒明，云雾不精，则上应白露不下。交通不表，万物命故不施，不施则名木多死。恶气不发，风雨不节，白露不下，则菀槁不荣。贼风数至，暴雨数起，天地四时不相保，与道相失，则未央绝灭。唯圣人从之，故身无奇病，万物不失，生气不竭。

逆春气，则少阳不生，肝气内变。逆夏气，则太阳不长，心气内洞。逆秋气，则太阴不收，肺气焦满。逆冬气，则少阴不藏，肾气独沉。夫四时阴阳者，万物之根本也，所以圣人春夏养阳，秋冬养阴，以从其根，故与万物沉浮于生长之门。逆其根，则伐其本，坏其真矣。故阴阳四时者，万物之终始也，死生之本也，逆之则灾害生，从之则苛疾不起，是谓得道。道者，圣人行之，愚者佩之。从阴阳则生，逆之则死，从之则治，逆之则乱。反顺为逆，是谓内格。是故圣人不治已病治未病，不治已乱治未乱，此之谓也。夫病已成而后药之，乱已成而后治之，譬犹渴而穿井，斗而铸锥，不亦晚乎﹗

\chapter{无极太极第三}
\section{太极拳论}
太极者，无极而生，动静之机，阴阳之母也。动之则分，静之则合。无过不及，随曲就伸。人刚我柔谓之「走」，我顺人背谓之「粘」。动急则急应，动缓则缓随。虽变化万端，而理唯一贯。 由着熟而渐悟懂劲，由懂劲而阶及神明。然非用功之久，不能豁然贯通焉！

虚领顶劲，气沉丹田，不偏不倚，忽隐忽现。左重则左虚，右重则右杳。仰之则弥高，俯之则弥深。进之则愈长，退之则愈促。一羽不能加，蝇虫不能落。人不知我，我独知人。英雄所向无敌，盖皆由此而及也！

斯技旁门甚多，虽势有区别，概不外壮欺弱、慢让快耳！有力打无力，手慢让手快。是皆先天自然之能，非关学力而有为也！察「四两拨千斤」之句，显非力胜；观耄耋能御众之形，快何能为？

立如平准，活似车轮。偏沉则随，双重则滞。每见数年纯功，不能运化者，率皆自为人制，双重之病未悟耳！欲避此病，须知阴阳。粘即是走，走即是粘。阴不离阳，阳不离阴。阴阳相济，方为懂劲。懂劲后愈练愈精，默识揣摩，渐至从心所欲。

本是「舍己从人」，多误「舍近求远」。所谓「差之毫厘，谬之千里」，学者不可不详辨焉！是为论。

\section{道德经}

01.道，可道也，非恒道也。名，可名也，非恒名也。无名，万物之始也；有名，万物之母也。故恒无欲也，以观其妙；恒有欲也，以观其所徼。两者同出，异名同谓，玄之又玄，众妙之门。

02.天下皆知美之为美，斯恶已；皆知善之为善，斯不善已。有无相生，难易相成，长短相形，高下相盈，音声相和，前后相随，恒也。是以圣人处无为之事，行不言之教。万物作而弗始，为而弗恃，功成而弗居。夫唯弗居，是以弗去。

03.不上贤，使民不争。不贵难得之货，使民不为盗。不见可欲，使民不乱。是以圣人之治也，虚其心，实其腹，弱其志，强其骨。恒使民无知无欲，使夫知不敢，弗为而已，则无不治矣。

04.道冲，而用之或不盈。渊兮！似万物之宗。锉其兑，解其纷，和其光，同其尘。湛兮似或存。吾不知其谁之子，象帝之先。

05.天地不仁，以万物为刍狗；圣人不仁，以百姓为刍狗。天地之间，其犹橐龠乎？虚而不屈，动而愈出。多闻数穷，不若守中。

06.谷神不死，是谓玄牝。玄牝之门，是谓天地之根。绵绵呵其若存！用之不堇。

07.天长地久。天地所以能长且久者，以其不自生，故能长生。是以圣人后其身而身先，外其身而身存，非以其无私邪？故能成其私。

08.上善若水。水善利万物而不争，处众人之所恶，故几于道。居善地，心善渊，予善仁，言善信，正善治，事善能，动善时。夫唯不争，故无尤。

09.持而盈之，不如其已。揣而锐之，不可长保。金玉满堂，莫之能守；富贵而骄，自遗其咎。功遂身退，天之道也。

10.载营魄抱一，能无离乎？专气致柔，能婴儿乎？涤除玄鉴，能无疵乎？爱民治国，能无知乎？天门开阖，能为雌乎？明白四达，能无为乎？生之畜之，生而弗有，长而弗宰也，是谓玄德。

11.三十辐共一毂，当其无，有车之用。埏埴以为器，当其无，有器之用。凿户牖以为室，当其无，有室之用。故有之以为利，无之以为用。

12.五色令人目盲，五音令人耳聋，五味令人口爽，驰骋畋猎令人心发狂，难得之货令人行妨。是以圣人之治也，为腹不为目，故去彼取此。

13.宠辱若惊，贵大患若身。何谓宠辱若惊？宠之为下也。得之若惊，失之若惊，是谓宠辱若惊。何谓贵大患若身？吾所以有大患者，为吾有身也；及吾无身，吾有何患？故贵为身于为天下，若可寄天下；爱以身为天下，若可托天下。

14.视之不见名曰微，听之不闻名曰希，捪之不得名曰夷。三者不可致诘，故混而为一。一者，其上不皦，其下不昧，绳绳兮不可名，复归于无物。是谓无状之状，无物之象，是谓惚恍。迎之不见其首，随之不见其后。执今之道，以御今之有，能知古始，是谓道纪。

15.古之善为道者，微妙玄通，深不可识。夫唯不可识，故强为之容曰：豫兮若冬涉川，犹兮若畏四邻，俨兮其若客，涣兮其若凌释，敦兮其若朴，旷兮其若谷，混兮其若浊。孰能浊以静之徐清？孰能安以静之徐生？保此道不欲盈，夫唯不欲盈，是以能蔽而不成。

16.致虚极，守静笃，万物并作，吾以观复。夫物芸芸，各复归其根。归根曰静，静，是谓复命。复命常也，知常明也。不知常，妄，妄作，凶。知常容，容乃公，公乃王，王乃天，天乃道，道乃久，没身不殆。

17.太上，下知有之。其次，亲而誉之。其次，畏之。其次，侮之。信不足，安有不信。悠兮，其贵言。成功遂事，而百姓谓我自然。

18.大道废，安有仁义。智慧出，安有大伪。六亲不和，安有孝慈。国家昏乱，安有忠臣。

19.绝圣弃知，民利百倍。绝仁弃义，民复孝慈。绝巧弃利，盗贼无有。此三言也，以为文不足，故令之有所属。见素抱朴，少私寡欲，绝学无忧。

20.唯之与阿，相去几何？美之与恶，相去若何？人之所畏，亦不可以不畏人。望兮，其未央哉！众人熙熙，如享太牢，如春登台。我独泊兮，其未兆，如婴儿之未咳。傫傫兮，若无所归。众人皆有余，我独遗。我愚人之心也哉，沌沌兮。俗人昭昭，我独昏昏。俗人察察，我独闷闷。澹兮，其若海。飂兮，若无所止。众人皆有以，而我独顽以鄙。我独异于人，而贵食母。

21.孔德之容，惟道是从。道之物，惟恍惟惚。惚兮恍兮，其中有象。恍兮惚兮，其中有物。窈兮冥兮，其中有情。其情甚真，其中有信。自今及古，其名不去，以阅众甫。吾何以知众甫之状哉？以此。

22.企者不立，自是者不彰，自见者不明，自伐者无功，自矜者不长。其在道也，曰余食赘形，物或恶之，故有道者不处。

23.曲则全，枉则直，洼则盈，敝则新，少则得，多则惑。是以圣人执一，以为天下牧。不自是故彰，不自见故明，不自伐故有功，弗矜故能长。夫唯不争，故莫能与之争。古之所谓曲则全者，岂虚言哉！诚全而归之。

24.希言自然，飘风不终朝，暴雨不终日。孰为此？天地而弗能久，又况于人乎！故从事而道者同于道，德者同于德，失者同于失。同于德者，道亦德。同于失者，道亦失之。

25.有物混成，先天地生。寂兮寥兮，独立而不改，可以为天地母。吾未知其名，字之曰道，强为之名曰大。大曰逝，逝曰远，远曰反。道大，天大，地大，王亦大。域中有四大，而王居一焉。人法地，地法天，天法道，道法自然。

26.重为轻根，静为躁君。是以君子终日行，不离辎重。虽有营观，燕处超然。奈何万乘之王，而以身轻天下？轻则失本，躁则失君。

27.善行者无辙迹，善言者无瑕谪，善数者不用筹策，善闭者无关楗而不可开，善结者无绳约而不可解。是以圣人恒善救人，而无弃人，物无弃财，是谓袭明。故善人，善人之师；不善人，善人之资也。不贵其师，不爱其资，虽知乎大迷，是谓妙要。

28.知其雄，守其雌，为天下溪。为天下溪，恒德不离。恒德不离，复归于婴儿。知其荣，守其辱，为天下谷。为天下谷，恒德乃足。恒德乃足，复归于朴。知其白，守其黑，为天下式。为天下式，恒德不忒。恒德不忒，复归于无极。朴散则为器，圣人用则为官长，夫大制无割。

29.将欲取天下而为之，吾见其不得已。天下神器，不可为也。为者败之，执者失之。故物或行或随，或嘘或吹，或强或羸，或培或墮。是以圣人去甚、去奢、去泰。

30.以道佐人主者，不以兵强天下，其事好还。师之所处，荆棘生焉。善者果而已，毋以取强焉。果而毋骄，果而勿矜，果而勿伐，果而毋得已居，是谓果而不强。物壮则老，是谓不道，不道早已。

31.夫隹兵者，不祥之器也。物或恶之，故有道者不处。君子居则贵左，用兵则贵右。故兵者非君子之器，兵者不祥之器也，不得已而用之，恬淡为上。勿美也，若美之，是乐杀人也。夫乐杀人，不可以得志于天下矣。是以吉事上左，丧事上右。是以偏将军居左，上将军居右。言以丧礼处之。杀人众，以悲哀泣之。战胜，以丧礼处之。

32.道恒无名，朴虽小，而天下弗敢臣。候王若能守之，万物将自宾。天地相合，以降甘露，民莫之令而自均。始制有名，名亦既有，夫亦将知止，知止所以不殆。譬道之在天下，犹小谷之于江海也。

33.知人者知，自知者明。胜人者有力，自胜者强。知足者富，强行者有志，不失其所者久，死而不亡者寿。

34.道汜兮，其可左右也。成功遂事而弗名有也。万物归焉而弗为主，则恒无欲也，可名于小。万物归焉而弗为主，可名为大。是以圣人之能成大也，以其不为大也，故能成大。

35.执大象，天下往。往而不害，安平太。乐与饵，过客止，道之出言，淡乎其无味。视之不足见，听之不足闻，用之不足既。

36.将欲翕之，必固张之。将欲弱之，必固强之。将欲去之，必固举之。将欲夺之，必固予之。是谓微明。柔弱胜强。鱼不可脱于渊，国之利器不可以示人。

37.道恒无名。候王若能守之，万物将自化。化而欲作，吾将镇之以无名之朴。镇之以无名之朴，夫将不欲。不欲以静，天地将自正。

% 德经
38.上德不德，是以有德；下德不失德，是以无德。上德无为而无以为。上仁为之而无以为。上义为之而有以为。上礼为之而莫之应，则攘臂而扔之。故失道而后德，失德而后仁，失仁而后义，失义而后礼。夫礼者，忠信之薄，而乱之首也。前识者，道之华，而愚之首也。是以大丈夫居其厚而不居其薄，居其实而不居其华。故去彼取此。

39.昔之得一者，天得一以清，地得一以宁，神得一以灵，谷得一以盈，万物得一以生，候王得一以为天下正。其诫之也，谓天毋已清将恐裂，地毋已宁将恐废，神毋已灵将恐歇，谷毋已盈将恐竭，候王毋已贵以高将恐蹶。故必贵以贱为本，必高以下为基。是以候王自称孤寡不谷。此其贱之本与，非也？故致数誉无誉。是故不欲禄禄若玉，硌硌若石。

40.上士闻道，勤而行之。中士闻道，若存若亡。下士闻道，大笑之。弗矣，不足以为道。故建言有之：明道若昧，进道若退，夷道若纇。上德若谷，大白若辱。广德若不足，建德若偷。质真若渝。大方无隅，大器免成。大音希声，大象无形，道襃无名。夫唯道，善始且善成。

41.反者道之动，弱者道之用。天下之物生于有，有生于无。

42.道生一，一生二，二生三，三生万物。万物负阴而抱阳，冲气以为和。人之所恶，唯孤寡不谷，而王公以自名。物或损之而益，或益之而损。人之所教，我亦教之。强梁者不得其死，吾将以为教父。

43.天下之至柔，驰骋天下之至坚。无有入无间，吾是以知无为之有益。不言之教，无为之益，天下希及之。

44.名与身孰亲？身与货孰多？得与亡孰病？甚爱必大费，多藏必厚亡。故知足不辱，知止不殆，可以长久。

45.大成若缺，其用不弊。大盈若冲，其用不穷。大直若屈，大巧若拙，大赢若肭。躁胜寒，静胜热，清静可以为天下正。

46.天下有道，却走马以粪。天下无道，戎马生于郊。罪莫大于可欲，祸莫大于不知足。咎莫憯于欲得。故知足之足，恒足矣。

47.不出于户，以知天下。不窥于牖，以知天道。其出弥远，其知弥少。是以圣人不行而知，不见而明，不为而成。

48.为学日益，为道日损。损之又损，以至于无为，无为而无以为。取天下，恒无事。及其有事，不足以取天下。

49.圣人恒无心，以百姓之心为心。善者善之，不善者亦善之，德善也。信者信之，不信者亦信之，德信也。圣人之在天下也，歙歙焉为天下浑心。百姓皆注其耳目，圣人皆孩之。

50.出生入死。生之徒十有三，死之徒十有三。而民生生，动皆之死地之十有三。夫何故？以其生生也。盖闻善摄生者，陵行不避兕虎，入军不被甲兵。兕无所投其角，虎无所措其爪，兵无所容其刃。夫何故？以其无死地焉。

51.道生之，德畜之，物形之而器成之。是以万物尊道而贵德。道之尊，德之贵也，夫莫之爵，而恒自然也。道生之、畜之、长之、育之、亭之、毒之、养之、覆之。生而弗有，为而弗恃，长而弗宰，是谓玄德。

52.天下有始，以为天下母。既得其母，以知其子。既知其子，复守其母，没身不殆。塞其兑，闭其门，终身不勤。启其兑，济其事，终身不救。见小曰明，守柔曰强。用其光，复归其明，无遗身殃，是为袭常。

53.使我挈有知，行于大道，唯迤是畏。大道甚夷，民甚好径。朝甚除，田甚芜，仓甚虚。服文采，带利剑，厌饮食，而资财有余。是谓盗竽。非道也哉！

54.善建者不拔，善抱者不脱，子孙以祭祀不绝。修之身，其德乃真。修之家，其德有余。修之乡，其德乃长。修之国，其德乃丰。修之天下，其德乃博。以身观身，以家观家，以乡观乡，以邦观邦，以天下观天下。吾何以知天下然哉？以此。

55.含德之厚，比于赤子。蜂蠆虺蛇不螫，攫鸟猛兽不搏。骨弱筋柔而握固，未知牝牡之合而朘怒，精之至也。终日号而不嗄，和之至也。知和曰常，知常曰明，益生曰祥，心使气曰强。物壮则老，谓之不道，不道早已。

56.知者不言，言者不知。塞其兑，闭其门，和其光，同其尘，挫其锐，解其纷，是谓玄同。故不可得而亲，亦不可得而疏；不可得而利，亦不可得而害；不可得而贵，亦不可得而贱。故为天下贵。

57.以正治国，以奇用兵，以无事取天下。吾何以知其然也哉？夫天下多忌讳，而民弥贫。民多利器，而国家滋昏。人多知巧，而奇物滋起。法物滋彰，而盗贼多有。是以圣人之言曰：我无为而民自化，我好静而民自正，我无事而民自富，我欲不欲而民自朴。

58.其政闷闷，其民淳淳。其政察察，其民夬夬。祸兮，福之所倚；福兮，祸之所伏。孰知其极？其无正也，正复为奇，善复为妖，人之迷也，其日固久。是以方而不割，廉而不刿，直而不肆，光而不耀。

59.治人事天莫若啬。夫唯啬，是以早服。早服是谓重积德。重积德则无不克。无不克则莫知其极。莫知其极，可以有国。有国之母，可以长久。是谓深根固柢，长生久视之道也。

60.治大国若烹小鲜，以道莅天下，其鬼不神。非其鬼不神也，其神不伤人也。非其神不伤人也，圣人亦弗伤也。夫两不相伤，故德交归焉。

61.大邦者，下流也，天下之牝也。天下之交也，牝恒以静胜牡。为其静也，故宜为下也。故大国以下小国，则取小国；小国以下大国，则取于大国。故或下以取，或下而取。大国者，不过欲兼畜人。小国者，不过欲入事人。夫皆得其欲，大者宜为下。

62.道者万物之注，善人之宝，不善人之所保。美言可以市，尊行可以加人。人之不善，何弃之有？故立天子，置三卿，虽有拱璧以先驷马，不若坐而进此。古之所以贵此者何也？不谓求以得，有罪以免与！故为天下贵。

63.为无为，事无事，味无味，大小，多少，报怨以德。图难于其易，为大于其细。天下之难作于易，天下之大作于细。是以圣人终不为大，故能成其大。夫轻诺必寡信，多易必多难。是以圣人犹难之，故终无难矣。

64.其安易持，其未兆易谋，其脆易泮，其微易散，为之于未有，治之于未乱。合抱之木，生于毫末。九层之台，起于累土。百仞之高，始于足下。为者败之，执者失之。是以圣人无为故无败，无执故无失。民之从事，恒于几成而败之。慎终如始，则无败事。是以圣人欲不欲，而不贵难得之货；学不学，而复众人之所过；能辅万物之自然，而弗敢焉。

65.古之为道者，非以明民，将以愚之也。夫民之难治，以其知也。故以知治国，国之贼也；不以知治国，国之福也。恒知此两者，亦稽式也。恒知稽式，是谓玄德。玄德深矣，远矣，与物反矣，乃至大顺。

66.江海所以能为百谷王者，以其善下之，故能为百谷王。是以圣人欲上民，必以言下之；欲先民，必以身后之。是居上而民弗重也，居前而民弗害也。天下乐推而弗厌也。非以其无争与？故天下莫能与争。

67.小国寡民，使有十百人之器而勿用，使民重死而远徙。有舟车无所乘之，有甲兵无所陈之，使民复结绳而用之。甘美食，美其服，乐其俗，安其居，邻国相望，鸡犬之声相闻，民至老死不相往来。

68.信言不美，美言不信。知者不博，博者不知。善者不多，多者不善。圣人不积，既以为人，己愈有；既以与人，己愈多。故天之道，利而不害；人之道，为而不争。

69.天下皆谓我大，大而不肖。夫唯不肖，故能大。若肖，久矣其细也夫。我恒有三宝，持而宝之。一曰慈，二曰俭，三曰不敢为天下先。夫慈，故能勇；俭，故能广；不敢为天下先，故能为器长。今舍慈且勇，舍俭且广，舍后且先，死矣！夫慈，以战则胜，以守则固。天将建之，如以慈垣之。

70.善为士者不武，善战者不怒，善胜敌者不与，善用人者为之下。是谓不争之德，是谓用人，是谓配天，古之极也。

71.用兵有言曰：吾不敢为主而为客，吾不敢进寸而退尺。是谓行无行，攘无臂，执无兵，乃无敌。祸莫大于无敌，无敌近亡吾宝。故抗兵相若，则哀者胜矣。

72.吾言甚易知，甚易行。而天下莫之能知，莫之能行也。言有宗，事有君。夫唯无知，是以不我知。知我者希，则我贵矣。是以圣人被褐而怀玉。

73.知不知，尚矣；不知知，病矣。是以圣人之不病，以其病病，是以不病。

74.民之不畏威，则大威将至矣。毋狭其所居，毋厌其所生。夫唯弗厌，是以不厌。是以圣人自知不自见，自爱不自贵。故去彼取此。

75.勇于敢则杀，勇于不敢则活。此两者或利或害，天之所恶，孰知其故？天之道，不争而善胜，不言而善应，不召而自来，坦然而善谋。天网恢恢，疏而不失。

76.若民恒且不畏死，奈何以杀惧之也？若民恒且畏死，而为奇者吾得而杀之，夫孰敢矣。若民恒且必畏死，则恒有司杀者。夫代司杀者杀，是代大匠斫也。夫代大匠斫者，则希不伤其手矣。

77.人之饥也，以其取食税之多，是以饥。百姓之不治也，以其上之有以为也，是以不治。民之轻死也，以其求生之厚也，是以轻死。夫唯无以生为者，是贤贵生。

78.人之生也柔弱，其死也筋肕坚强。万物草木之生也柔脆，其死也枯槁。故曰：坚强死之徒，柔弱生之徒也。是以兵强则不胜，木强则烘。故强大居下，柔弱居上。

79.天之道，犹张弓也。高者抑下，下者举之，有余者损之，不足者补之。故天之道，损有余而补不足。人之道则不然，损不足而奉有余。夫孰能有余而有以取奉于天者？唯有道者乎。是以圣人为而弗有，成功而弗居也，若此其不欲见贤也。

80.天下莫柔弱于水，而攻坚强者莫之能胜，以其无以易之也。柔之胜刚，弱之胜强，天下莫不知，而莫能行也。是以圣人云：受国之垢，是谓社稷主；受国之不祥，是谓天下王。正言若反。

81.和大怨，必有余怨，安可以为善？是以圣人执右契，而不以责于人。故有德司契，无德司彻。夫天道无亲，恒与善人。

% 01.道可道，非常道。名可名，非常名。无名天地之始。有名万物之母。故常无欲以观其妙。常有欲以观其徼。此两者同出而异名，同谓之玄。玄之又玄，众妙之门。

% 02.天下皆知美之为美，斯恶矣；皆知善之为善，斯不善已。故有无相生，难易相成，长短相形，高下相倾，音声相和，前後相随。是以圣人处无为之事，行不言之教。万物作焉而不辞，生而不有，为而不恃，功成而弗居。夫唯弗居，是以不去。

% 03.不尚贤，使民不争。不贵难得之货，使民不为盗。不见可欲，使民心不乱。是以圣人之治，虚其心，实其腹，弱其志，强其骨；常使民无知、无欲，使夫智者不敢为也。为无为，则无不治。

% 04.道冲而用之，或不盈。渊兮似万物之宗。挫其锐，解其纷，和其光，同其尘，湛兮似或存。吾不知谁之子，象帝之先。

% 05.天地不仁，以万物为刍狗。圣人不仁，以百姓为刍狗。天地之间，其犹橐龠乎？虚而不屈，动而愈出。多言数穷，不如守中。

% 06.谷神不死是谓玄牝。玄牝之门是谓天地根。绵绵若存，用之不勤。

% 07.天长地久。天地所以能长且久者，以其不自生，故能长生。是以圣人後其身而身先，外其身而身存。非以其无私邪。故能成其私。

% 08.上善若水。水善利万物而不争，处众人之所恶，故几於道。居善地，心善渊，与善仁，言善信，政善治，事善能，动善时。夫唯不争，故无尤。

% 09.持而盈之不如其己；揣而锐之不可长保；金玉满堂莫之能守；富贵而骄，自遗其咎。功遂身退，天之道。

% 10.载营魄抱一，能无离乎？专气致柔，能如婴儿乎？涤除玄览，能无疵乎？爱国治民，能无为乎？天门开阖，能为雌乎？明白四达，能无知乎。生之、畜之，生而不有，為而不恃，長而不宰。是謂玄德。

% 11.三十幅共一毂，当其无，有车之用。埏埴以为器，当其无，有器之用。凿户牖以为室，当其无，有室之用。故有之以为利，无之以为用。

% 12.五色令人目盲，五音令人耳聋，五味令人口爽，驰骋畋猎令人心发狂，难得之货令人行妨。是以圣人，为腹不为目，故去彼取此。

% 13.宠辱若惊，贵大患若身。何谓宠辱若惊？宠为下。得之若惊失之若惊，是谓宠辱若惊。何谓贵大患若身？吾所以有大患者，为吾有身，及吾无身，吾有何患。故贵以身为天下，若可寄天下。爱以身为天下，若可托天下。

% 14.视之不见名曰夷。听之不闻名曰希。抟之不得名曰微。此三者不可致诘，故混而为一。其上不皦，其下不昧，绳绳不可名，复归於无物。是谓无状之状，无物之象，是谓惚恍。迎之不见其首，随之不见其後。执古之道以御今之有。能知古始，是谓道纪。 %(jiǎo)

% 15.古之善为士者，微妙玄通，深不可识。夫唯不可识，故强为之容。豫兮若冬涉川；犹兮若畏四邻；俨兮其若容；涣兮若冰之将释；敦兮其若朴；旷兮其若谷；混兮其若浊。孰能浊以静之徐清？孰能安以动之徐生？保此道者不欲盈。夫唯不盈故能蔽而不新成。 %飉兮

% 16.致虚极，守静笃。万物并作，吾以观复。夫物芸芸，各复归其根。归根曰静，是谓复命；复命曰常，知常曰明。不知常，妄作凶。知常容，容乃公，公乃全，全乃天，天乃道，道乃久，没身不殆。

% 17.太上，不知有之。其次，亲而誉之。其次，畏之。其次，侮之。信不足焉，有不信焉。悠兮其贵言，功成事遂，百姓皆谓我自然。

% 18.大道废有仁义；慧智出有大伪；六亲不和有孝慈；国家昏乱有忠臣。 

% 19.绝圣弃智，民利百倍；绝仁弃义，民复孝慈；绝巧弃利，盗贼无有；此三者，以为文不足。故令有所属，见素抱朴，少私寡欲。

% 20.绝学无忧，唯之与阿，相去几何？善之与恶，相去若何？人之所畏，不可不畏。荒兮其未央哉！众人熙熙，如享太牢，如春登台。我独泊兮其未兆，如婴儿之未孩；儡儡兮若无所归。众人皆有馀，而我独若遗。我愚人之心也哉！沌沌兮。俗人昭昭，我独昏昏；俗人察察，我独闷闷。众人皆有以，而我独顽且鄙。我独异於人，而贵食母。

% 21.孔德之容惟道是从。道之为物惟恍惟惚。惚兮恍兮其中有象。恍兮惚兮其中有物。窈兮冥兮其中有精。其精甚真。其中有信。自古及今，其名不去以阅众甫。吾何以知众甫之状哉！以此。

% 22.曲则全，枉则直，洼则盈，敝则新少则得，多则惑。是以圣人抱一为天下式。不自见故明；不自是故彰；不自伐故有功；不自矜故长；夫唯不争，故天下莫能与之争。古之所谓「曲则全者」岂虚言哉！诚全而归之。

% 23.希言自然。故飘风不终朝，骤雨不终日。孰为此者？天地。天地尚不能久，而况於人乎？故从事於道者，道者同於道。德者同於德。失者同於失。同於道者，道亦乐得之；同於德者，德亦乐得之；同於失者，失於乐得之。信不足焉，有不信焉。

% 24.企者不立，跨者不行。自见者不明，自是者不彰。自伐者无功，自矜者不长。其在道也，曰：馀食赘形。物或恶之，故有道者不处。

% 25.有物混成，先天地生。寂兮寥兮，独立而不改，周行而不殆，可以为天下母。吾不知其名，字之曰道。强为之名曰大。大曰逝，逝曰远，远曰反。故道大、天大、地大、人亦大。域中有四大，而人居其一焉。人法地，地法天，天法道，道法自然。

% 26.重为轻根，静为躁君。是以君子终日行，不离辎重。虽有荣观燕处超然。奈何万乘之主而以身轻天下。轻则失根，躁则失君。

% 27.善行无辙迹。善言无瑕谪。善数不用筹策。善闭无关楗而不可开。善结无绳约而不可解。是以圣人常善救人，故无弃人。常善救物，故无弃物。是谓袭明。故善人者，不善人之师。不善人者，善人之资。不贵其师、不爱其资，虽智大迷，是谓要妙。

% 28.知其雄，守其雌，为天下溪。为天下溪，常德不离，复归於婴儿。知其白，守其黑，为天下式。为天下式，常德不忒，复归於无极。知其荣，守其辱，为天下谷。为天下谷，常德乃足，复归於朴。朴散则为器，圣人用之则为官长。故大制不割。

% 29.将欲取天下而为之，吾见其不得已。天下神器，不可为也，为者败之，执者失之。夫物或行或随、或觑或吹、或强或羸、或挫或隳。是以圣人去甚、去奢、去泰。

% 30.以道佐人主者，不以兵强天下。其事好还。师之所处荆棘生焉。大军之後必有凶年。善有果而已，不敢以取强。果而勿矜。果而勿伐。果而勿骄。果而不得已。果而勿强。物壮则老，是谓不道，不道早已。

% 31.夫佳兵者不祥之器，物或恶之，故有道者不处。君子居则贵左，用兵则贵右。兵者不祥之器，非君子之器，不得已而用之，恬淡为上。胜而不美，而美之者，是乐杀人。夫乐杀人者，则不可得志於天下矣。吉事尚左，凶事尚右。偏将军居左，上将军居右。言以丧礼处之。杀人之众，以悲哀泣之，战胜以丧礼处之。

% 32.道常无名。朴虽小，天下莫能臣也。侯王若能守之，万物将自宾。天地相合以降甘露，民莫之令而自均。始制有名，名亦既有，夫亦将知止，知止可以不殆。譬道之在天下，犹川谷之於江海。

% 33.知人者智，自知者明。胜人者有力，自胜者强。知足者富。强行者有志。不失其所者久。死而不亡者寿。

% 34.大道泛兮，其可左右。万物恃之以生而不辞，功成而不名有。衣养万物而不为主，常无欲可名於小。万物归焉，而不为主，可名为大。以其终不自为大，故能成其大。

% 35.执大象天下往。往而不害安平太。乐与饵，过客止。道之出口淡乎其无味。视之不足见。听之不足闻。用之不足既。

% 36.将欲歙之，必固张之。将欲弱之，必固强之。将欲废之，必固兴之。将欲取之，必固与之。是谓微明。柔弱胜刚强。鱼不可脱於渊，国之利器不可以示人。

% 37.道常无为，而无不为。侯王若能守之，万物将自化。化而欲作，吾将镇之以无名之朴。无名之朴，夫亦将无欲。不欲以静，天下将自定。

% 38.上德不德是以有德。下德不失德是以无德。上德无为而无以为。下德无为而有以为。上仁为之而无以为。上义为之而有以为。上礼为之而莫之以应，则攘臂而扔之。故失道而後德。失德而後仁。失仁而後义。失义而後礼。夫礼者忠信之薄而乱之首。前识者，道之华而愚之始。是以大丈夫，处其厚不居其薄。处其实，不居其华。故去彼取此。

% 39.昔之得一者。天得一以清。地得一以宁。神得一以灵。谷得一以盈。万物得一以生。侯王得一以为天下贞。其致之。天无以清将恐裂。地无以宁将恐发。神无以灵将恐歇。谷无以盈将恐竭。万物无以生将恐灭。侯王无以贞将恐蹶。故贵以贱为本，高以下为基。是以侯王自谓孤、寡、不谷。此非以贱为本邪？非乎。至誉无誉。不欲琭琭如玉，珞珞如石。

% 40.反者道之动。弱者道之用。天下万物生於有，有生於无。 

% 41.上士闻道勤而行之。中士闻道若存若亡。下士闻道大笑之。不笑不足以为道。故建言有之。明道若昧。进道若退。夷道若纇。上德若谷。大白若辱。广德若不足。建德若偷。质真若渝。大方无隅。大器晚成。大音希声。大象无形。道隐无名。夫唯道善贷且成。

% 42.道生一。一生二。二生三。三生万物。万物负阴而抱阳，冲气以为和。人之所恶，唯孤、寡不谷，而王公以为称，故物或损之而益，或益之而损。人之所教，我亦教之，强梁者，不得其死。吾将以为教父。

% 43.天下之至柔，驰骋天下之至坚。无有入无间，吾是以知无为之有益。不言之教，无为之益天下希及之。

% 44.名与身孰亲。身与货孰多。得与亡孰病。是故甚爱必大费。多藏必厚亡。知足不辱。知止不殆。可以长久。

% 45.大成若缺，其用不弊。大盈若冲，其用不穷。大直若屈。大巧若拙。大辩若讷。静胜躁，寒胜热。清静为天下正。

% 46.天下有道，却走马以粪。天下无道，戎马生於郊。罪莫大于可欲。祸莫大於不知足。咎莫大於欲得。故知足之足常足矣。

% 47.不出户知天下。不窥牖见天道。其出弥远，其知弥少。是以圣人不行而知。不见而明。不为而成。 

% 48.为学日益。为道日损。损之又损，以至於无为。无为而不为。取天下常以无事，及其有事，不足以取天下。

% 49.圣人无常心。以百姓心为心。善者吾善之。不善者吾亦善之，德善。信者吾信之。不信者吾亦信之，德信。圣人在天下，歙歙焉，为天下浑其心。百姓皆注其耳目，圣人皆孩之。 %(xīxī,无所偏执的样子)

% 50.出生入死。生之徒，十有三。死之徒，十有三。人之生，动之於死地，亦十有三。夫何故？以其生生之厚。盖闻善摄生者，陆行不遇兕虎，入军不被甲兵。兕无所投其角。虎无所用其爪。兵无所容其刃。夫何故？以其无死地。

% 51.道生之，德畜之，物形之，势成之。是以万物莫不尊道，而贵德。道之尊，德之贵，夫莫之命而常自然。故道生之，德畜之。长之育之。成之熟之。养之覆之。生而不有，为而不恃，长而不宰。是谓玄德。

% 52.天下有始，以为天下母。既得其母，以知其子。既知其子，复守其母，没身不殆。塞其兑，闭其门，终身不勤。开其兑，济其事，终身不救。见其小曰明，守柔曰强。用其光，复归其明，无遗身殃。是为袭常。

% 53.使我介然有知，行於大道，唯施是畏。大道甚夷，而人好径。朝甚除，田甚芜，仓甚虚。服文彩，带利剑，厌饮食，财货有馀。是谓盗夸。非道也哉。

% 54.善建者不拔。善抱者不脱。子孙以祭祀不辍。修之於身其德乃真。修之於家其德乃馀。修之於乡其德乃长。修之於邦其德乃丰。修之於天下其德乃普。故以身观身，以家观家，以乡观乡，以邦观邦，以天下观天下。吾何以知天下然哉？以此。

% 55.含德之厚比於赤子。毒虫不螫，猛兽不据，攫鸟不抟。骨弱筋柔而握固。未知牝牡之合而朘作，精之至也。终日号而不嗄，和之至也。知和曰常。知常曰明。益生曰祥。心使气曰强。物壮则老。谓之不道，不道早已。

% 56.知者不言。言者不知。塞其兑，闭其门，挫其锐，解其纷，和其光，同其尘，是谓玄同。故不可得而亲。不可得而疏。不可得而利。不可得而害。不可得而贵。不可得而贱。故为天下贵。

% 57.以正治国，以奇用兵，以无事取天下。吾何以知其然哉？以此。天下多忌讳而民弥贫。民多利器国家滋昏。人多伎巧奇物滋起。法令滋彰盗贼多有。故圣人云我无为而民自化。我好静而民自正。我无事而民自富。我无欲而民自朴。

% 58.其政闷闷，其民淳淳。其政察察，其民缺缺。祸尚福之所倚。福兮祸之所伏。孰知其极，其无正邪？正复为奇，善复为妖。人之迷，其日固久。是以圣人方而不割。廉而不刿。直而不肆。光而不耀。

% 59.治人事天莫若啬。夫唯啬是谓早服。早服谓之重积德。重积德则无不克。无不克则莫知其极。莫知其极可以有国。有国之母可以长久。是谓深根固柢，长生久视之道。

% 60.治大国若烹小鲜。以道莅天下，其鬼不神。非其鬼不神，其神不伤人。非其神不伤人，圣人亦不伤人。夫两不相伤，故德交归焉。

% 61.大国者下流，天下之交。天下之牝。牝常以静胜牡。以静为下。故大国以下小国，则取小国。小国以下大国，则取大国。故或下以取，或下而取。大国不过欲兼畜人。小国不过欲入事人。夫两者各得所欲，大者宜为下。

% 62.道者万物之奥。善人之宝，不善人之所保。美言可以市尊。尊行可以加人。人之不善，何弃之有。故立天子、置三公，虽有拱璧以先驷马，不如坐进此道。古之所以贵此道者何。不曰求以得，有罪以免邪？故为天下贵。

% 63.为无为，事无事，味无味。大小多少，报怨以德。图难於其易，为大於其细。天下难事必作於易。天下大事必作於细。是以圣人终不为大，故能成其大。夫轻诺必寡信。多易必多难。是以圣人犹难之，故终无难矣。

% 64.其安易持，其未兆易谋。其脆易破，其微易散。为之於未有，治之於未乱。合抱之木生於毫末。九层之台起於累土。千里之行始於足下。为者败之，执者失之。是以圣人无为故无败，无执故无失。民之从事常於几成而败之。慎终如始则无败事。是以圣人欲不欲，不贵难得之货。学不学，复众人之所过，以辅万物之自然而不敢为。

% 65.古之善为道者，非以明民，将以愚之。民之难治，以其智多。故以智治国，国之贼。不以智治国，国之福。知此两者，亦楷式。常知楷式，是谓玄德。玄德深矣、远矣！与物反矣。然後乃至大顺。

% 66.江海之所以能为百谷王者，以其善下之，故能为百谷王。是以圣人欲上民，必以言下之。欲先民，必以身後之。是以圣人处上而民不重，处前而民不害。是以天下乐推而不厌。以其不争，故天下莫能与之争。

% 67.天下皆谓我道大，似不肖。夫唯大，故似不肖。若肖，久矣！其细也夫。我有三宝，持而保之：一曰慈， 二曰俭，三曰不敢为天下先。慈故能勇，俭故能广，不敢为天下先故能成器长。今舍慈且勇，舍俭且广，舍後且先，死矣！夫慈以战则胜，以守则固。天将救之以慈卫之。

% 68.善为士者不武。善战者不怒。善胜敌者不与。善用人者为之下。是谓不争之德。是谓用人之力。是谓配天古之极。

% 69.用兵有言，吾不敢为主而为客。不敢进寸而退尺。是谓行无行。攘无臂。执无兵。乃无敌。祸莫大於轻敌。轻敌几丧吾宝。故抗兵相加哀者胜矣。

% 70.吾言甚易知、甚易行。天下莫能知、莫能行。言有宗、事有君。夫唯无知，是以我不知。知我者希，则我者贵。是以聖人被褐怀玉。

% 71.知不知上，不知知病。夫唯病病，是以不病。圣人不病，以其病病。夫唯病病，是以不病。

% 72.民不畏威，则大威至。无狎其所居，无厌其所生。夫唯不厌，是以不厌。是以圣人自知不自见。自爱不自贵。故去彼取此。

% 73.勇於敢则杀。勇於不敢则活。此两者或利或害。天之所恶，孰知其故。天之道，不争而善胜。不言而善应。不召而自来。繟然而善谋。天网恢恢，疏而不失。 %(chǎn,舒缓)

% 74.民不畏死，奈何以死惧之。若使民常畏死，而为奇者，吾得执而杀之，孰敢。常有司杀者杀。夫代司杀者杀，是谓代大匠斫。夫代大匠斫者，希有不伤其手矣。

% 75.民之饥以其上食税之多，是以饥。民之难治以其上之有为，是以难治。民之轻死以其求生之厚，是以轻死。夫唯无以生为者，是贤於贵生。

% 76.人之生也柔弱，其死也坚强。草木之生也柔脆，其死也枯槁。故坚强者死之徒，柔弱者生之徒。是以兵强则不勝，木强则折。强大处下，柔弱处上。

% 77.天之道其犹张弓与。高者抑之，下者举之。有馀者损之，不足者补之。天之道，损有馀而补不足。人之道，则不然，损不足以奉有馀。孰能有馀以奉天下，唯有道者。是以圣人为而不恃，功成而不处。其不欲见贤邪！

% 78.天下莫柔弱於水。而攻坚强者，莫之能胜。以其无以易之。弱之胜强。柔之胜刚。天下莫不知莫能行。是以圣人云，受国之垢，是谓社稷主。受国不祥，是为天下王。正言若反。

% 79.和大怨，必有馀怨，安可以为善。是以圣人执左契，而不责於人。有德司契，无德司彻。天道无亲，常与善人。

% 80.小国寡民。使有什伯之器而不用。使民重死而不远徙。虽有舟舆无所乘之。虽有甲兵无所陈之。使民复结绳而用之。甘其食、美其服、安其居、乐其俗。邻国相望，鸡犬之声相闻。民至老死不相往来。

% 81.信言不美。美言不信。善者不辩。辩者不善。知者不博。博者不知。圣人不积。既以为人己愈有。既以与人己愈多。天之道，利而不害。圣人之道，为而不争。

\newpage

\section{清静经}
老君曰。大道无形。生育天地。大道无情。运行日月。大道无名。长养万物。吾不知其名。强名曰道。夫道者。有清有浊。有动有静。天清地浊。天动地静。男清女浊。男动女静。降本流末。而生万物。清者浊之源。动者静之基。人能常清静。天地悉皆归。

夫人神好清。而心扰之。人心好静。而欲牵之。常能遣其欲。而心自静。澄其心而神自清。自然六欲不生。三毒消灭。所以不能者。为心未澄。欲未遣也。能遣之者。内观其心。心无其心。外观其形。形无其形。远观其物。物无其物。三者既悟。唯见於空。观空亦空。空无所空。所空既无。无无亦无。无无既无。湛然常寂。寂无所寂。欲岂能生。欲既不生。即是真静。真常应物。真常得性。常应常静。常清静矣。如此清静。渐入真道。既入真道。名为得道。虽名得道。实无所得。为化众生。名为得道。能悟之者。可传圣道。

老君曰。上士无争。下士好争。上德不德。下德执德。执著之者。不名道德。众生所以不得真道者。为有妄心。既有妄心。即惊其神。既惊其神。即著万物。既著万物。即生贪求。即生贪求。即是烦恼。烦恼妄想。忧苦身心。但遭浊辱。流浪生死。常沉苦海。永失真道。

真常之道。悟者自得。得悟道者。常清静矣。

\chapter{藏象经络第四}
\section{脏腑经络}
医之初，脏腑详，未入室，先升堂。

曰肝心，脾肺肾，此五脏，五行蕴。\\
胃膀胱，大小肠，三焦胆，六腑囊。\\
小肠心，大肠肺，胆联肝，脾联胃，\\
肾膀胱，表里配，胞与焦，十二位。

心为主，出聪明；肺为辅，治节行；\\
肝将军，主谋虑；脾仓廪，出五味；\\
肾作强，出伎巧；焦决渎，主水道；\\
膻中臣，出喜乐；膀州都，津液蓄；\\
大传导，变化出；小受盛，主化物；\\
胆决断，十二官；心明暗，定危安。

脏与腑，外属经；手与足，二六均。\\
足太阳，属膀胱。足阳明，胃腑当。\\
足少阳，属胆腑。足太阴，脾所主。\\
足少阴，内属肾。足厥阴，肝之本。\\
手太阳，为小肠。手阳明，是大肠。\\
手少阳，三焦强。手太阴，即肺金。\\
手少阴，内属心。手厥阴，胞络经。\\
经脏明，内外分。内脏腑，外络经。\\
外为阳，内为阴。营卫畅，血气平。\\
五脏安，六经宁。

肝属木，火属心，脾属土，肺属金，\\
肾属水，五行生。火生土，土生金，\\
金生水，水何能？水生木，木乃荣。\\
木生火，火克金；金制木，木不横；\\
木制土，土平敦；土制水，水不淫；\\
水制火，火不焚。子母辨，主从分。\\
宜活用，辨证真。

肝主筋，主颜色；心主臭，主血脉；\\
脾主味，主肌肉；肾主液，亦主骨；\\
肺主声，主皮毛。此五主，各有条。

肝藏魂，亦藏血；肺藏气，兼藏魄；\\
脾统血，藏意津；心生血，主藏神；\\
肾藏志，亦藏精。此五藏，医宜明。

肝窍目，心窍舌，肾窍耳，肺窍鼻，\\
脾之窍，上下唇。此五窍，各有门。\\
肝色青，肺色白，脾色黄，肾色黑，\\
心色赤，此五色，观部位，论生克。\\
心液汗，肺液涕，脾液涎，肝液泪，\\
肾液唾，此五液，谷之生，肾之泽。\\
呼属肝，笑属心，肺声哭，肾声呻，\\
脾声歌，此五声，司之肺，参五音。\\
肺臭腥，肝臭臊，肾臭腐，心臭焦，\\
脾臭香，鼻自招，此五臭，心所操。\\
心味苦，肝味酸，肾味咸，脾味甘，\\
肺味辛，见口间，此五味，脾所关。\\
心志喜，肺志悲，肝志怒，脾志思，\\
肾志恐，意自知，此五志，肾所司。\\
心部胸，肺背脊，肾部腰，肝部胁，\\
脾部腹，四肢属，此五部，各有别。\\
须属肾，发属心，皮毛肺，脾腋阴，\\
眉属肝，五脏生，此五者，各当分。\\

肝沉弦，心大散，肺浮涩，脾缓见，\\
肾沉濡，三指按，此五脉，诊宜辨。\\
心左寸，肝左关，肾与命，两尺间，\\
肺右寸，脾右关，六腑配，静心参。\\
有任督，百脉王。前与后，分两行。\\
前行短，后行长。滞则病，通则常。

肝王春，职主生；风吹物，动以惊。\\
心王夏，长为主；热鼓物，蒸以暑。\\
肺王秋，主收成；露凝物，物成形。\\
肾王冬，职藏真；能滋润，固本根。\\
人身气，上应天；阴与阳，不可偏。

肝运血，动则行；气太过，中风名。\\
心扬神，宣则明；气太过，伤暑云。\\
肺凝气，聚则生；气太过，伤寒称。\\
肾藏精，润不枯；气太过，中湿呼。\\
脾纳谷，化液津，养脏腑，运周身；\\
气太过，疾病生。此五证，本气病。\\
非外感，乃内因。

邪五传，各有根；病自作，正邪称。\\
我克来，微邪轻；克我来，贼邪深。\\
实邪来，子传亲；亲传子，虚邪名。\\
顺逆晓，死生分；察其原，究其因。\\
病七传，症可畏，逢时克，医须忌。

七情伤，五损致，法不同，治亦异。\\
肺伤皮，宜益气；心伤血，调营卫；\\
脾损肉，肌不生，调饮食，适寒温；\\
肝损筋，缓其中；肾损骨，益其精。\\
略明脏，再论经。

\section{六经标本}

足太阳，寒水经，主皮肤，护周身。\\
表中表，统卫营。行身后，脉上循，\\
头项肩，腰背臀。六经首，一身藩。\\
外邪来，此先干，受诸病，名伤寒。\\
他经病，由兹传，法早治，表即痊。\\
其本寒，其标热，标本从，三纲别。\\
小雪后，大寒前。司天岁，辰戌年。

足阳明，属燥金，主肌肉，行前身。\\
表中里，部明堂，鼻同额，两目眶。\\
其本燥，其标阳，化从中，治清凉。\\
表宜解，里宜清，腑症成，下之灵。\\
秋分后，立冬先，卯酉岁，主司天。

足少阳，相火经。行身侧，职主筋。\\
脉循胁，络耳根。标本同，表里均，\\
和为贵，虚实论。腑属火，火宜清。\\
司天岁，在寅申。小满暑，主气存。

足太阴，是湿土。里中表，腹内主。\\
化从本，寒热生。温下法，理中军。\\
脉布胃，有传经，有直中，误下分。\\
司天岁，未与丑。完白露，起大暑。

足少阴，身本根。里中表，表里均。\\
循喉舌，金水联。其本热，其标寒。\\
病从化，有二门，寒回阳，热救阴。\\
春分起，立夏完。逢子午，司天年。

足厥阴，风木经。里中里，阴中阴。\\
络于肝，循宗筋，阴丽极，阳将生。\\
其本热，其标寒，化从中，治惟难。\\
阴阳混，寒热兼；寒热判，温凉悬。\\
逢巳亥，主气年。大寒起，惊蛰圆。

六经法，详《伤寒》，合并病，单双传。\\
五脏明，六经宣，宗扁仲，是真诠。\\
有逐流，症千般；能循本，源独探。\\
括诸病，十一端；参活法，应用宽。

\section{十二经纳地支歌}
%\chooseFrom{明·杨继洲}{针灸大成}

肺寅大卯胃辰宫，
脾巳心午小未中，\\
申胱酉肾心包戌，
亥焦子胆丑肝通。

\chapter{修身第五}

\section{豫诚堂家训}
% \chooseFrom{清·刘沅}{槐轩全书}

天理良心，人之所以为人；宽仁厚德，覆载所以长久。昧良悖理，不得为人；褊心小量，安能合天？得天理以为人，天地故为父母；父母才有我身，父母故同天地。欺堂上父母易，欺头上父母难。一念欺天，即为不孝；一念欺亲，得罪于天。修道以谕亲，尊父母如天地也；尽性而参赞，事天地如父母也。孝在修德，德在修心。移孝可以作忠，只为不欺不肆；静存始能动察，必须无怠无荒。犯了邪淫，便是禽兽；喜欢势利，定成鄙夫。保养作善，即守身诚身之义；知非改过，为希贤希圣之门。人生如梦，修善修福方长；大道难逢，父教师教为本。自心抱愧，说甚夫纲父纲；作事不真，怎样为臣为子？治天下无多术，养教周全；学圣贤有何难，恕道便好。勤职业，修心术，何患饥寒？贪财色，乱人伦，必戕身命。弟兄以仁让为主，正家以夫妇为先。饱暖平安，是为清福；温良恭俭，到处春风。读书要读好书，凡事必宗孔孟；作人要作好人，时刻敬畏神天。善为儿孙积财，莫如积德；多行巧诈害己，安能害人？先代格言甚多，在乎身体；圣人事业何在？必先正心。私欲去而聪明始开，致知故先格物；念头好而是非分明，实践乃为诚意。养心养气，小效亦可延年；成己成人，功夫全在大学。道须深造，功在返求。在上不正其趋，人才从何而出？伦常本于心性，故曰一以贯之。学业骛于浮华，所以万事堕矣！戒之勉之，庶乎不替祖训。


\section{大学}
大学之道，在明明德，在亲民，在止于至善。知止而后有定，定而后能静，静而后能安，安而后能虑，虑而后能得。物有本末，事有终始，知所先后，则近道矣。 %（qīn）

古之欲明明德于天下者，先治其国；欲治其国者，先齐其家；欲齐其家者，先修其身；欲修其身者，先正其心；欲正其心者，先诚其意；欲诚其意者，先致其知；致知在格物。

物格而后知至，知至而后意诚，意诚而后心正，心正而后身修，身修而后家齐，家齐而后国治，国治而后天下平。

自天子以至于庶人，壹是皆以修身为本。其本乱，而末治者否矣。其所厚者薄，而其所薄者厚，未之有也。此谓知本，此谓知之至也。%% （shù）

所谓诚其意者，毋自欺也。如恶恶臭，如好好色，此之谓自慊。故君子必慎其独也。小人闲居为不善，无所不至。见君子而后厌然，掩其不善，而著其善。人之视己，如见其肺肝然，则何益矣？此谓诚于中，形于外。故君子必慎其独也。曾子曰「十目所视，十手所指，其严乎！」富润屋，德润身，心广体胖，故君子必诚其意。

诗云「瞻彼淇澳，绿竹猗猗。有斐君子，如切如磋，如琢如磨。瑟兮僩兮！赫兮喧兮！有斐君子，终不可谖兮」。如切如磋者，道学也。如琢如磨者，自修也。瑟兮僩兮者，恂栗也。赫兮喧兮者，威仪也。有斐君子，终不可谖兮者，道盛德至善，民之不能忘也。

诗云「於戏！前王不忘」。君子贤其贤而亲其亲，小人乐其乐而利其利，此以没世不忘也。康诰曰「克明德」。大甲曰「顾諟天之明命」。帝典曰「克明峻德」。皆自明也。

汤之盘铭曰「苟日新，日日新，又日新」。康诰曰「作新民」。诗云「周虽旧邦，其命惟新」。是故君子无所不用其极。

诗云「邦畿千里，惟民所止」。诗云「缗蛮黄鸟，止于丘隅」。子曰「于止，知其所止，可以人而不如鸟乎？」诗云「穆穆文王，於缉熙敬止」。为人君，止于仁。为人臣，止于敬。为人子，止于孝。为人父，止于慈。与国人交，止于信。子曰「听讼，吾犹人也。必也使无讼乎！」无情者，不得尽其辞。大畏民志，此谓知本。

所谓修身在正其心者：身有所忿懥，则不得其正；有所恐惧，则不得其正；有所好乐，则不得其正；有所忧患，则不得其正；心不在焉，视而不见，听而不闻，食而不知其味。此谓修身在正其心。

所谓齐其家在修其身者：人之其所亲爱而辟焉，之其所贱恶而辟焉，之其所畏敬而辟焉，之其所哀矜而辟焉，之其所敖惰而辟焉。故好而知其恶，恶而知其美者，天下鲜矣。故谚有之曰「人莫知其子之恶。莫知其苗之硕」。此谓身不修，不可以齐其家。

所谓治国必齐其家者，其家不可教，而能教人者，无之。故君子不出家，而成教于国。

孝者，所以事君也。弟者，所以事长也。慈者，所以使众也。康诰曰「如保赤子」。心诚求之，虽不中，不远矣。未有学养子，而后嫁者也。一家仁，一国兴仁；一家让，一国兴让；一人贪戾，一国作乱；其机如此。此谓一言偾事，一人定国。

尧舜率天下以仁，而民从之。桀纣率天下以暴，而民从之。其所令反其所好，而民不从。是故君子有诸己，而后求诸人。无诸己，而后非诸人。所藏乎身不恕，而能喻诸人者，未之有也。故治国在齐其家。

诗云「桃之夭夭，其叶蓁蓁。之子于归，宜其家人」。宜其家人，而后可以教国人。诗云「宜兄宜弟」。宜兄宜弟，而后可以教国人。诗云「其仪不忒，正是四国」。其为父子兄弟足法，而后民法之也。此谓治国在齐其家。

所谓平天下在治其国者：上老老，而民兴孝；上长长，而民兴弟；上恤孤，而民不倍。是以君子有絜矩之道也。所恶于上，毋以使下；所恶于下，毋以事上；所恶于前，毋以先后；所恶于后，毋以从前；所恶于右，毋以交于左；所恶于左，毋以交于右；此之谓絜矩之道。

诗云「乐只君子，民之父母」。民之所好好之，民之所恶恶之，此之谓民之父母。诗云「节彼南山，维石岩岩。赫赫师尹，民具尔瞻」。有国者不可以不慎；辟，则为天下僇矣。

诗云「殷之未丧师，克配上帝。仪监于殷，峻命不易」。道得众则得国，失众则失国。是故君子先慎乎德；有德此有人，有人此有土，有土此有财，有财此有用。德者，本也；财者，末也。外本内末，争民施夺。是故财聚则民散，财散则民聚。是故言悖而出者，亦悖而入；货悖而入者，亦悖而出。

康诰曰「惟命不于常」。道善则得之，不善则失之矣。楚书曰「楚国无以为宝，惟善以为宝」。舅犯曰「亡人无以为宝，仁亲以为宝」。秦誓曰「若有一介臣，断断兮，无他技，其心休休焉，其如有容焉；人之有技，若己有之；人之彦圣，其心好之，不啻若自其口出；寔能容之。以能保我子孙黎民，尚亦有利哉！人之有技，媢嫉以恶之；人之彦圣，而违之俾不通；以不能保我子孙黎民，亦曰殆哉！」

唯仁人放流之，迸诸四夷，不与同中国。此谓唯仁人为能爱人，能恶人。见贤而不能举，举而不能先，命也；见不善而不能退，退而不能远，过也。好人之所恶，恶人之所好，是谓拂人之性，灾必逮夫身。是故君子有大道，必忠信以得之，骄泰以失之。

生财有大道，生之者众，食之者寡，为之者疾，用之者舒，则财恒足矣。仁者以财发身，不仁者以身发财。未有上好仁，而下不好义者也；未有好义，其事不终者也；未有府库财，非其财者也；孟献子曰「畜马乘，不察于鸡豚；伐冰之家，不畜牛羊；百乘之家，不畜聚敛之臣，与其有聚敛之臣，宁有盗臣」。此谓国不以利为利，以义为利也。长国家而务财用者，必自小人矣。彼为善之。小人之使为国家，灾害并至，虽有善者，亦无如之何矣。此谓国不以利为利，以义为利也。


%% \section*{十二经相传次序歌}
%% 肺大胃脾心小肠，

%% 膀肾包焦胆肝续，

%% 手阴脏手阳手头，

%% 足阴足腹阳头足。



%% \section{十二经脉歌}
%% 手太阴肺中焦生，下络大肠出贲门，上膈属肺从肺系，系横出腋臑中行。肘臂寸口上鱼际，大指内侧爪甲根，支络还从腕后出，接次指属阳明经。此经多气而少血，是动则病喘与咳，肺胀膨膨缺盆痛，两手交瞀为臂厥。所生病者为气嗽，喘渴烦心胸满结，臑臂之内前廉痛，小便频数掌中热。气虚肩背痛而寒，气盛亦疼风汗出，欠伸少气不足息，遗矢无度溺色赤。
%% 阳明之脉手大肠，次指内侧起商阳，循指上连出合谷，两筋歧骨循臂肪。入肘外廉循臑外，肩端前廉柱骨旁，从肩下入缺盆内，络肺下膈属大肠。支从缺盆直上颈，斜贯颊前下齿当，环出人中交左右，上侠鼻孔注迎香。此经气盛血亦盛，是动臑肿并齿痛；所生病者为鼽衄，目黄口干喉痹生。大指次指难为用，肩前臑外痛相仍，气有余兮脉热肿，虚则寒栗病偏增。
%% 胃足阳明交鼻起，下循鼻外下入齿，还出侠口绕承浆，颐后大迎颊车里。耳前发际至额颅，支下人迎缺盆底，下膈入胃络脾宫，直者缺盆下乳内。一支幽门循腹中，下行直合气冲逢，遂由髀关抵膝膑，腨跗中指内关同。一支下膝注三里，前出中指外关通，一支别走足跗趾，大趾之端经尽矣。此经多气复多血，是动欠伸面颜黑。凄凄恶寒畏见人，忽闻木音心惊惕，登高而歌弃衣走，甚则腹胀仍贲响。凡此诸疾皆骭厥，所生病者为狂疟，温淫汗出鼻流血，口喎唇裂又喉痹，膝膑疼痛腹胀结，气膺伏兔胻外廉，足跗中趾俱痛彻，有余消谷溺色黄，不足身前寒振栗，胃房胀满食不消，气盛身前皆有热。
%% 太阴脾起足大趾，上循内侧白肉际，核骨之后内踝前，上腨循胻胫膝里。股内前廉入腹中，属脾络胃与膈通，侠喉连舌散舌下，支络从胃注心宫。此经气盛而血衰，是动其病气所为，食入即吐胃脘痛，更兼身体痛难移，腹胀善噫舌本强，得后与气快然衰。所生病者舌亦痛，体重不食亦如之，烦心心下仍急痛，泄水溏瘕寒疟随，不卧强立股膝肿，疸发身黄大指痿。
%% 手少阴脉起心中，下膈直与小肠通，支者还从肺系走，直上喉咙系目瞳。直者上肺出腋下，臑后肘内少海从，臂内后廉抵掌中，锐骨之端注少冲。多气少血属此经，是动心脾痛难任，渴欲饮水咽干燥，所生胁痛目如金，臑臂之内后廉痛，掌中有热向经寻。
%% 手太阳经小肠脉，小指之端起少泽，循手外廉出踝中，循臂骨出肘内侧。上循臑外出后廉，直过肩解绕肩胛，交肩下入缺盆内，向腋络心循咽嗌。下膈抵胃属小肠，一支缺盆贯颈颊，至目锐眦却入耳，复从耳前仍上颊，抵鼻升至目内眦，斜络于颧别络接。此经少气还多血，是动则病痛咽嗌，颔下肿兮不可顾，肩如拔兮臑似折。所生病主肩臑痛，耳聋目黄肿腮颊，肘臂之外后廉痛，部分犹当细分别。
%% 足太阳经膀胱脉，目内眦上起额尖，支者巅上至耳角，直者从巅脑后悬。络脑还出别下项，仍循肩膊侠脊边，抵腰膂肾膀胱内，一支下与后阴连。贯臀斜入委中穴，一支膊内左右别，贯胛侠脊过髀枢，臀内后廉中合，下贯腨内外踝后，京骨之下指外侧。此经血多气犹少，是动头疼不可当，项如拔兮腰似折，髀枢痛彻脊中央，腘如结兮腨如裂，是为踝厥筋乃伤。所生疟痔小指废，头囟顶痛目色黄，腰尻腘脚疼连背，泪流鼻衄及癫狂。
%% 足经肾脉属少阴，小指斜趋涌泉心，然骨之下内踝后，别入跟中腨内侵。出腨内廉上股内，贯脊属肾膀胱临，直者属肾贯肝膈，入肺循喉舌本寻；支者从肺络心内，仍至胸中部分深。此经多气而少血，是动病饥不欲食，喘嗽唾血喉中鸣，坐而欲起面如垢，目视佴佴气不足，心悬如饥常惕惕。所生病者为舌干，口热咽痛气贲逼，股内后廉并脊疼，心肠烦痛疸而澼，痿厥嗜卧体怠惰，足下热痛皆肾厥。
%% 手厥阴心主起胸，属包下膈三焦宫，支者循胸出胁下，胁下连腋三寸同。仍上抵腋循臑内，太阴、少阴两经中，指透中冲支者别，小指次指络相通。此经少气原多血，是动则病手心热，肘臂挛急腋下肿，甚则胸胁支满结。心中澹澹或大动，善笑目黄面赤色，所生病者为烦心，心痛掌热病之则。
%% 手经少阳三焦脉，起自小指次指端，两指歧骨手腕表，上出臂外两骨间。肘后臑外循肩上，少阳之后交别传，下入缺盆膻中分，散络心包膈里穿。支者膻中缺盆上，上项耳后耳角旋，屈下至颐仍注颊，一支出耳入耳前，却从上关交曲颊，至目锐眦乃尽焉。此经少血还多气，是动耳鸣喉肿痹，所生病者汗自出，耳后痛兼目锐眦，肩臑肘臂外皆疼，小指次指亦如废。
%% 足脉少阳胆之经，始从两目锐眦生，抵头循角下耳后，脑空风池次第行。手少阳前至肩上，交少阳右上缺盆，支者耳后贯耳内，出走耳前锐眦循。一支锐眦大迎下，合手少阳抵雜根，下加颊车缺盆合，入胸贯膈络肝经。属胆仍从胁里过，下入气冲毛际萦，横入髀厌环跳内，直者缺盆下腋膺。过季胁下髀厌内，出膝外廉是阳陵，外辅绝骨踝前过，足跗小趾次趾分。一支别从大趾去，三毛之际接肝经。此经多气而少血，是动口苦善太息，心胁疼痛难转移，面尘足热体无泽。所生头痛连锐眦；缺盆肿痛并两腋，马刀挟瘿生两旁，汗出振寒痎疟疾，胸胁髀膝至胫骨，绝骨踝痛及诸节。
%% 厥阴足脉肝所终，大指之端毛际丛，足跗上廉太冲分，踝前一寸入中封。上踝交出太阴后，循腨内廉阴股冲，环绕阴器抵小腹，侠胃属肝络胆逢。上贯膈里布胁肋，侠喉颃颡目系同，脉上巅会督脉出，支者还生目系中，下络颊里环唇内，支者便从膈肺通。此经血多气少焉，是动腰疼俯仰难，男疝女人小腹肿，面尘脱色及咽干。所生病者为胸满，呕吐洞泄小便难，或时遗溺并狐疝，临症还须仔细看。
